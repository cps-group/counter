package io.github.cpsgroup.hypereton.plugin.counter;

import io.github.cpsgroup.hypereton.plugin.Plugin;

/**
 * Interface for counter plugins.
 * Implementing classes must be annotated with org.springframework.stereotype.Component in order for plugin loading
 * to work.
 * <p/>
 * Created by Manuel Weidmann on 06.09.2014.
 */
public interface Counter extends Plugin {

    /**
     * Returns the number of sections this counter has found.
     *
     * @return the number of sections
     */
    public int getNumberOfSections();

    /**
     * Returns the number of pages this counter has found.
     *
     * @return the number of pages
     */
    public int getNumberOfPages();

    /**
     * Returns the number of words this counter has found.
     *
     * @return the number of words
     */
    public int getNumberOfWords();

}
